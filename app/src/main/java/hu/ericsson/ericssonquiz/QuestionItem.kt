package hu.ericsson.ericssonquiz

import hu.ericsson.ericssonquiz.data.Question

class QuestionItem {
    lateinit var questionText: String
    lateinit var answerItems : MutableList<AnswerItem>
    var questionAnswered = false

    constructor(question: Question) : super() {

        this.questionText = question.question

        answerItems = mutableListOf(AnswerItem(question.goodAnswer, true),
            AnswerItem(question.wrongAnswerA, false),
            AnswerItem(question.wrongAnswerB, false),
            AnswerItem(question.wrongAnswerC, false))
        answerItems.shuffle()
    }

    fun questionAnswered(i: Int) {
        questionAnswered = true
        answerItems[i].isClicked = true
    }

    data class AnswerItem (val text: String, var goodAnswer : Boolean, var isClicked : Boolean = false)
}