package hu.ericsson.ericssonquiz.data

import android.view.View
import androidx.recyclerview.widget.RecyclerView

data class Question (val question : String,
                     val goodAnswer : String,
                     val wrongAnswerA: String,
                     val wrongAnswerB: String,
                     val wrongAnswerC: String)