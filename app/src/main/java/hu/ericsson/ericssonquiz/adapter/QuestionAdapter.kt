package hu.ericsson.ericssonquiz.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import hu.ericsson.ericssonquiz.QuestionItem
import hu.ericsson.ericssonquiz.R
import kotlinx.android.synthetic.main.question_item.view.*

class QuestionAdapter(val items: List<QuestionItem>): RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.question_item, parent, false
        )
        return QuestionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {
        holder.tvQuestion.text = items[position].questionText

        for (btn in holder.btnList) {

            var i = holder.btnList.indexOf(btn)
            val answerItem = items[holder.adapterPosition].answerItems[i]
            btn.text = answerItem.text
            btn.alpha = 1f
            btn.setBackgroundColor(Color.rgb(118, 118, 118))

            if(items[holder.adapterPosition].questionAnswered) {
                btn.isClickable = false
                colorButton(btn, answerItem, holder.adapterPosition)
            } else {
                btn.setOnClickListener() {
                    items[holder.adapterPosition].questionAnswered(holder.btnList.indexOf(btn))
                    for (button in holder.btnList) {
                        button.isClickable = false
                        colorButton(button, items[holder.adapterPosition].answerItems[holder.btnList.indexOf(button)], position)
                    }
                }
            }
        }
   }

    private fun colorButton(
        btn: Button,
        answerItem: QuestionItem.AnswerItem,
        position: Int
    ){
        if (answerItem.goodAnswer and answerItem.isClicked) {
            btn.alpha = 1f
            btn.setBackgroundColor(Color.rgb(15, 195, 115))
        } else if (answerItem.goodAnswer) {
            btn.alpha = 0.4f
            btn.setBackgroundColor(Color.rgb(15, 195, 115))
        } else if (answerItem.isClicked) {
            btn.alpha = 1f
            btn.setBackgroundColor(Color.rgb(255, 50, 50))
        }
    }

    class QuestionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
       val tvQuestion = itemView.question

       val btnList = listOf<Button>(itemView.btnAnswerA,
           itemView.btnAnswerB,
           itemView.btnAnswerC,
           itemView.btnAnswerD)
   }
}