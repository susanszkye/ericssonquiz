package hu.ericsson.ericssonquiz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hu.ericsson.ericssonquiz.adapter.QuestionAdapter
import hu.ericsson.ericssonquiz.data.Question
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: QuestionAdapter

    val questions : List<Question> = mutableListOf(
        Question("For how long has Ericsson been developing TITAN?","17", "2", "5", "8"),
        Question("Under what licence was TITAN made open source?", "Eclipse Public License (EPL)", "Common Public Licence (CPL)", "GNU General Public License (GPL)", "BSD-2-Clause"),
        Question("In what programming language can you write test cases in TITAN?", "TTCN-3", "Perl", "Erlang", "Tea"),
        Question("In what programming language can you write additions to your test code?", "C++ and Java", "C#/.NET", "PHP", "Python"),
        Question("How can your reach the TITAN open source community?", "Eclipse Titan Forum", "Telephone/Fax", "Bottle post", "SMS")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        val questionViewItems = mutableListOf<QuestionItem>()
        for(question in questions) {
            questionViewItems.add(QuestionItem(question))
        }


        adapter = QuestionAdapter(questionViewItems)
        recyclerQuiz.adapter = adapter

        btnReset.setOnClickListener {
            questionViewItems.clear()
            for(question in questions) {
                questionViewItems.add(QuestionItem(question))
            }

            questionViewItems.shuffle()

            adapter.notifyDataSetChanged()
        }
    }
}
